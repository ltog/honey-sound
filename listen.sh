#!/bin/bash

# Copyright 2016 Lukas Toggenburger; https://github.com/ltog/honey-sound
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


sudo tcpdump -l -n -i eth0 -e icmp[icmptype] == 8 2>&1 | grep --line-buffered -ie 'echo request' | xargs -L1 -P 10 ./play_ping.sh
