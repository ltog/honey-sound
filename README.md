# honey-sound

Make network traffic audible by acting as a honeypot

## Description

The goal of this software is to act as a honeypot and make incoming connection attempts perceivable by playing a sound. The current version only plays a sound for incoming ICMP echo requests.

## Installation

Under Ubuntu install `sox` and its codecs:

    sudo apt-get install sox libsox-fmt-all

## Usage

Start listening/playing by executing:

    ./listen.sh

The program should then play a sound for every incoming ICMP echo request.

## Technical notes

### Why wrapping `play` in `play_ping.sh`?

`xargs` is responsible for playing sounds. Since xargs wants to append arguments to executed commands and we have no use for the arguments, the wrapper script `play_ping.sh` is used.

## License

Unless noted otherwise the license for all files is GPLv3.

## Credits

The file `ping.wav` was created by digifishmusic, is licensed as CC BY 3.0 and originates from https://www.freesound.org/people/digifishmusic/sounds/42796/ .

The program was inspired by an acoustical visualization of nuclear bombs going off, launched by different nations. It might have been https://www.youtube.com/watch?v=LLCF7vPanrY , but I'm not sure anymore.
